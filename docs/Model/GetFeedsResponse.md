# GetFeedsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payload** | [**\Swagger\Client\Model\FeedList**](FeedList.md) |  | [optional] 
**next_token** | **string** | Returned when the number of results exceeds pageSize. To get the next page of results, call the getFeeds operation with this token as the only parameter. | [optional] 
**errors** | [**\Swagger\Client\Model\ErrorList**](ErrorList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


