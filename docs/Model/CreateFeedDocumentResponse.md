# CreateFeedDocumentResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payload** | [**\Swagger\Client\Model\CreateFeedDocumentResult**](CreateFeedDocumentResult.md) |  | [optional] 
**errors** | [**\Swagger\Client\Model\ErrorList**](ErrorList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


