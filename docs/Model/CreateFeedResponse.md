# CreateFeedResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payload** | [**\Swagger\Client\Model\CreateFeedResult**](CreateFeedResult.md) |  | [optional] 
**errors** | [**\Swagger\Client\Model\ErrorList**](ErrorList.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


